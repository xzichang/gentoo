# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

SRC_URI="https://github.com/iDvel/rime-ice/releases/download/2024.09.25/full.zip -> ${P}.zip"

DESCRIPTION="rime-ice"
HOMEPAGE="https://github.com/iDvel/rime-ice"

LICENSE="GPL-3"
SLOT="0"
KEYWORDS="amd64"

S=${WORKDIR}

src_install() {
	insinto /usr/share/rime-data/
	doins -r ${S}/*
}
