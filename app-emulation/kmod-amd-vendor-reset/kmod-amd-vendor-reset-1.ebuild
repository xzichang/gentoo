# Copyright 1999-2023 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit linux-mod-r1

EGIT_COMMIT="084881c6e9e11bdadaf05798e669568848e698a3"
SRC_URI="https://github.com/gnif/vendor-reset/archive/${EGIT_COMMIT}.tar.gz -> ${P}.tar.gz"

DESCRIPTION="Linux kernel vendor specific hardware reset module"
HOMEPAGE="https://github.com/gnif/vendor-reset"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="amd64"

CONFIG_CHECK="FTRACE KPROBES PCI_QUIRKS KALLSYMS FUNCTION_TRACER"


S=${WORKDIR}/vendor-reset-${EGIT_COMMIT}


src_compile() {
	local modlist=( vendor-reset )
	local modargs=( KDIR="${KV_OUT_DIR}" )
	linux-mod-r1_src_compile
}

src_install() {
	linux-mod-r1_src_install

	insinto /etc/modules-load.d/
	newins ${FILESDIR}/modload.conf vendor-reset.conf
	insinto /lib/udev/rules.d/
	doins ${S}/udev/99-vendor-reset.rules
}
